=== Checkout Manager ===
Contributors: al-imran-akash
Tags: woocommerce checkout manager, woocommerce checkout editor, woocommerce checkout fields editor, woocommerce, woocommerce checkout, checkout, checkout editor
Requires at least: 5.0
Tested up to: 5.9
Stable tag: 1.0.2
Requires PHP: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Checkout Manager - The most advanced and powerful customization of your checkout page.

== Description ==
Checkout Manager for WooCommerce is an excellent tool to increase your conversion rates and boost your sales.
It allows you to add, edit, customize, and delete fields on the checkout page.

== Installation ==
Installation is fairly straight forward. Install it from the WordPress plugin repository. 

== Screenshots ==
1. screenshot-1
2. screenshot-2
3. screenshot-3
4. screenshot-4
5. screenshot-5
6. screenshot-6
7. screenshot-7
8. screenshot-8
9. screenshot-9

== Changelog ==

= 1.0.2 - 2022-04-21 =
* [fix] Fixed error.

= 1.0.1 - 2022-04-20 =
* [fix] Add display position, style option and fixed display all custom fields in thankyou page, order details page and emails

= 1.0.0 - 2022-04-05 =
* [fix] Display fixed in thankyou page, order details page and emails

= 0.9 =
* Initial version release